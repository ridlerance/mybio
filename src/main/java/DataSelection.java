import dataselection.WorkExperience;
import dataselection.WorkResponsibilities;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Brooklin on 25.10.14.
 */
public class DataSelection implements Serializable {

    private DB db;
    private List<WorkExperience> list;
    private List<WorkResponsibilities> workResponsibilitiesList;

    public DataSelection(DB db) {

        this.db = db;
    }

    public DataSelection() {

        this.db = new DB();

    }


    public List<WorkExperience> readWorkExperience() {

        String request = getWorkExperienceRequest();

        ResultSet resultSet = db.getResulSet(request);

        list = new ArrayList<>();

        int id = 1;

        try {

            while (resultSet.next()) {

                WorkExperience workExperience = new WorkExperience(id);
                workExperience.setOrg(resultSet.getString("Org"));
                workExperience.setPosition(resultSet.getString("Position"));
                workExperience.setBeginDate(resultSet.getDate("BeginDate"));
                workExperience.setEndDate(resultSet.getDate("EndDate"));

                list.add(workExperience);
                id += 1;
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return list;
    }

    public List<WorkResponsibilities> readWorkResponsibilities() {

        String request = getWorkResposibilitiesRequest();

        ResultSet resultSet = db.getResulSet(request);

        workResponsibilitiesList = new ArrayList<WorkResponsibilities>();

        int id = 1;

        try {

            while (resultSet.next()) {

                WorkResponsibilities workResponsibilities = new WorkResponsibilities(id);
                workResponsibilities.setDescription(resultSet.getString("Description"));
                workResponsibilitiesList.add(workResponsibilities);
                id+=1;
            }

        } catch (SQLException ex) {

            ex.printStackTrace();

        }

        return workResponsibilitiesList;
    }

    private String getWorkExperienceRequest() {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("SELECT ");
        stringBuilder.append("db.Id,");
        stringBuilder.append("db.Org,");
        stringBuilder.append("db.Position,");
        stringBuilder.append("db.BeginDate,");
        stringBuilder.append("db.EndDate");
        stringBuilder.append(" FROM mybio_schema.work_experience as db");

        return stringBuilder.toString();

    }

    private String getWorkResposibilitiesRequest() {

        String request = "SELECT db.Id,db.description,db.workexperienceid " +
                "FROM mybio_schema.work_responsibilities as db";

        return request;
    }
}

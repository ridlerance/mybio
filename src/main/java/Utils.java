import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by Brooklin on 25.10.14.
 */
public class Utils {

    public static final String propertiesFileName = "project.properties";

    public static String getProperty(String propertyName) {
        Properties prop = new Properties();
        try {
            prop.load(Utils.class.getClassLoader().getResourceAsStream(propertiesFileName));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop.getProperty(propertyName);

    }
}

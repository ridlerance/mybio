package dataselection;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Brooklin on 25.10.14.
 */
@Named(value="WorkExperience")
@SessionScoped
public class WorkExperience implements Serializable {

    private int id;
    private String org;
    private String position;
    private Date beginDate;
    private Date endDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public WorkExperience(int id, String org, String position, Date beginDate, Date endDate) {

        this.id = id;
        this.org = org;
        this.position = position;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    public WorkExperience() {
    }

    public WorkExperience(int id) {
        this.id = id;
    }
}


package dataselection;

import sun.security.krb5.internal.crypto.Des;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by Brooklin on 27.10.14.
 */
@Named(value = "workResponsibilities")
@SessionScoped
public class WorkResponsibilities implements Serializable {

    private int id;
    private String Description;
    private int WorkExperienceId;

    public WorkResponsibilities(int id, String description) {
        this.id = id;
        Description = description;
    }

    public WorkResponsibilities() {
    }

    public WorkResponsibilities(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return Description;
    }

    public int getWorkExperienceId() {
        return WorkExperienceId;
    }

    public void setWorkExperienceId(int workExperienceId) {
        WorkExperienceId = workExperienceId;
    }

    public void setDescription(String description) {
        Description = description;
    }
}

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by Brooklin on 19.10.14.
 */

@Named(value = "user")

@SessionScoped
public class User implements Serializable {

    private String login;
    private String password;

    public User() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Named(value = "enter")
    public void enter(){

    }
}


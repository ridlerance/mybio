import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.sql.*;

/**
 * Created by Brooklin on 19.10.14.
 */
@Named
@SessionScoped
public class DB implements Serializable {

    private Connection connection;
    private Statement statement;

    public DB() {

        getConnection();
        getStatement();

    }

    private static void registClass(String driver) {

        try {

            Class.forName(driver);

        } catch (ClassNotFoundException ex) {

            System.out.println(ex.getMessage());

        }

    }

    public Connection getConnection() {

        String db_url = Utils.getProperty("db_url");
        String db_driver = Utils.getProperty("db_driver");
        String db_user = Utils.getProperty("db_user");
        String db_password = Utils.getProperty("db_password");

       // registClass(db_driver);

        try {

            connection = DriverManager.getConnection(db_url, db_user, db_password);

        } catch (SQLException ex) {

            System.out.println(ex.getMessage());

        }


        return connection;

    }

    public Statement getStatement() {

        try {
            statement = connection.createStatement();
        } catch (SQLException ex) {

            System.out.println(ex.getMessage());

        }

        return statement;

    }

    public ResultSet getResulSet(String request) {

        ResultSet resultSet = null;

        if (connection != null && statement != null) {

            try {

                resultSet = statement.executeQuery(request);

            } catch (SQLException ex) {

                System.out.println(ex.getMessage());
            }
        }

        return resultSet;
    }
}

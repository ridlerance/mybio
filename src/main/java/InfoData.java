import dataselection.WorkExperience;
import dataselection.WorkResponsibilities;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named(value = "infoDate")
@SessionScoped
public class InfoData implements Serializable {

    private List<WorkExperience> workExperienceList;
    private List<WorkResponsibilities> workResponsibilitiesList;

    @Inject
    DataSelection dataSelection;

    @PostConstruct
    public void init() {

        dataSelection = new DataSelection();
        workExperienceList = dataSelection.readWorkExperience();
        workResponsibilitiesList = dataSelection.readWorkResponsibilities();

    }

    public List<WorkExperience> getWorkExperienceList() {
        return workExperienceList;
    }

    public void setWorkExperienceList(List<WorkExperience> workExperienceList) {
        this.workExperienceList = workExperienceList;
    }

    public List<WorkResponsibilities> getWorkResponsibilitiesList() {

        return workResponsibilitiesList;
    }

    public void setWorkResponsibilitiesList(List<WorkResponsibilities> workResponsibilitiesList) {

        this.workResponsibilitiesList = workResponsibilitiesList;

    }

    @Inject
    public InfoData() {

        dataSelection = new DataSelection();
        workExperienceList = dataSelection.readWorkExperience();
        workResponsibilitiesList = dataSelection.readWorkResponsibilities();

    }
}
